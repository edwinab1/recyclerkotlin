package com.example.edwin.recyclermascotas

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import java.util.ArrayList

/**
 * Created by edwin on 12/7/2017.
 */
class MyAdapter(var context: Context, var cardlist: ArrayList<Card>) : RecyclerView.Adapter<MyAdapter.ListaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): ListaViewHolder {
        val li = LayoutInflater.from(parent.context)
        val vi = li.inflate(R.layout.activity_cardiew, parent, false)
        return ListaViewHolder(vi)
    }

    override fun onBindViewHolder(holder: ListaViewHolder, position: Int) {
        holder.title.text = cardlist[position].title
        holder.description.text = cardlist[position].description
        holder.dog.setImageResource(cardlist[position].itemImage)

    }

    override fun getItemCount(): Int {
        if (cardlist.isEmpty())
            return 0
        else
            return cardlist.size
    }

    inner class ListaViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var title: TextView
        internal var description: TextView
        internal var dog: ImageView

        init {
            title = view.findViewById(R.id.cardtitle) as TextView
            description = view.findViewById(R.id.carddescription) as TextView
            dog = view.findViewById(R.id.cardpicture) as ImageView
        }
    }
}
