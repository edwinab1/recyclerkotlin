package com.example.edwin.recyclermascotas

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    private val titles = arrayOf("Marlon", "Pepe", "Ludoviko", "Nano", "Choco")
    private val descriptions = arrayOf("se me perdio mi perrita... es una schnauzer color negro por arriba y gris por abajo... se me perdio en el barrio pastrana en neiva ", "el 10 de diciembre en las horas de la tarde, se extravío mi perrita en el barrio Tuquila, salio de la casa y por la bulla de la pólvora salio corriendo en el sur de Nieva. Muchas Gracias Leidy Villarreal", "Se perdió hace 12 días en el Barrio Vergel, se ofrece recompensa de 250 mil pesos", "se me salio de la casa tipo 6 de la tarde en un descuido y se me perdio en el barrio si lo vieron en un sitio de comidas rapidas por la 64 de las mercedes hay fue la ultima vez que lo vieron", "Salió hacia el medio día, por el Sector de Ipanema y sus alrededores, no llevaba collar, ni ningún tipo de identificación")
    private val pictures = intArrayOf(R.drawable.dog4, R.drawable.dog5, R.drawable.dog6, R.drawable.dog7, R.drawable.dog8)

    private var adaptador: MyAdapter? = null
    private val cardList = ArrayList<Card>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //method
        initCards()

        if (adaptador == null)
            adaptador = MyAdapter(this, cardList)

        val myrecyclerview = findViewById(R.id.myrecycler) as RecyclerView
        myrecyclerview!!.adapter = adaptador
        myrecyclerview!!.layoutManager = LinearLayoutManager(this)


    }

    private fun initCards() {
        for (i in titles.indices) {
            val mycard = Card()
            mycard.title = titles[i]
            mycard.description = descriptions[i]
            mycard.itemImage = pictures[i]
            cardList.add(mycard)

        }
    }
}
