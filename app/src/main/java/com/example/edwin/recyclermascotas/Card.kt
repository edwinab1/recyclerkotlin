package com.example.edwin.recyclermascotas

/**
 * Created by edwin on 12/7/2017.
 */
class Card {

    var title: String? = null
    var description: String? = null
    var itemImage: Int = 0
}
